# Kang is an open source JavaScript framework

## DOM Selector
### id selector
```javascript
$$('#icon');
```
### class
### tag

### CSS selector
```javascript
$$('#icon').css('backgroundPosition', '0 -50px');
```

## DOM effects
### tab
```javascript
$$('#tab').tab();
```

## Helper functions


### Code Example
```javascript
    (function() {
            $$('#remove').click(function() {
                $('#first').remove();
            });
            $$('#empty').click(function() {
                // $('#first').empty();
                $("#first").html('');   //fast then create element
            });
            $$('#tab').tab('click');
            
        })();
```
