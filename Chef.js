'use strict';

class Engineer {
  constructor(food) {
    this.food = food;
  }

  cook() {
    console.log(this.food);
  }
}

export Engineer;
