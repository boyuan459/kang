/**
 * Kang framework v1.0.0
 *
 * Kang framework is similar to jQuery, but more lightweight and applying more modern JavaScript concept
 * with object oriented
 *
 * if you want to use $$.bindTemplate() and $$.artTemplate functions, it requires arttemplate , please refer to http://aui.github.com/artTemplate/
 *
 * Copyright 2017 Kang
 * Released under the MIT license
 */

(function (w) {
    Kang = function (selector) {
        this.elements = [];
        if (typeof selector === 'object') {
            //wrap this $$(this)
            this.elements.push(selector);
            return this;
        } else if (selector.match(/^<(\w+)><\/(\w+)>$/)) {
            //create new element
            var tag = selector.replace(/^<(\w+)><\/(\w+)>$/g, function(match, key) {
                    return key;
                });
            var ele = document.createElement(tag);
            this.elements.push(ele);
            return this;
        }
        return this.$all(selector);
    };

    Kang.prototype = {
        //html5 query selector
        $all: function (selector, context) {
            context = context || document;
            this.elements = context.querySelectorAll(selector);
            return this;
        }
    };

    //create this object here only for using the extend method
    var $extend = function () { };

    $extend.prototype = {
        extend: function (tar, source) {
            for (var i in source) {
                tar[i] = source[i];
            }

            return tar;
        }

    };

    //instance the object to using its extend method
    $$extend = new $extend();
    $$ = function (id) {
        return new Kang(id);
    };

    /**
     * check data type framework
     */
    $$extend.extend(w.$$, {
        isString: function(val) {
            return typeof val === "string";
        },
        isNumber: function(val) {
            return typeof val === "number";
        },
        isBoolean: function(val) {
            return typeof val === "boolean";
        },
        isUndefined: function(val) {
            return typeof val === "undefined";
        },
        isObj: function(str) {
            if (str === null || typeof str === "undefined") {
                return false;
            }
            return typeof str === 'object';
        },
        isNull: function(str) {
            return val === null;
        },
        isArray: function(arr) {
            if (arr === null || typeof arr === 'undefined') {
                return false;
            }
            return arr.constructor === Array;
        }
    });

    $$extend.extend(w.$$, {
        on: function (id, type, fn) {
            var dom = w.$$.isString(id) ? document.getElementById(id) : id;
            if (dom.addEventListener) {
                dom.addEventListener(type, fn, false);
            } else if (dom.attachEvent) {
                //ie 6
                dom.attachEvent('on' + type, fn);
            }
        }
    });

    //base extend framework
    $$extend.extend(w.$$, {
        //extend target object with source object by copying all properties of source to target
        extend: function (tar, source) {
            for (var i in source) {
                tar[i] = source[i];
            }

            return tar;
        },
        //advance extend by copying multiple objects to target
        extend2: function() {
            var key, i = 0, len = arguments.length, target = null, copy;
            console.log(arguments);
            if (len == 0) {
                return;
            } else if (len === 1) {
                target = this;
            } else {
                i++;
                target = arguments[0];
            }
            for(;i<len;i++) {
                for(key in arguments[i]) {
                    copy = arguments[i][key];
                    target[key] = copy;
                }
            }

            return target;
        }
    });

    /**
     * common framework
     */
    $$extend.extend(w.$$, {
        queryString: function() {
            //get the query string from the url
            var str = window.location.search.substring(1);  //get the query string, id=1&name=byuan
            var arr = str.split('&');
            var json = {};
            for(var i=0;i<arr.length;i++) {
                var c = arr[i].indexOf('=');
                if (c == -1) continue;
                var key = arr[i].substring(0, c);
                var value = arr[i].substring(c+1);
                json[key] = value;
            }

            return json;
        }
    });

    /**
     * template framework, this require arttemplate , please refer to http://aui.github.com/artTemplate/
     */
    $$extend.extend(w.$$, {
        formatString: function (str, data) {
            return str.replace(/@\((\w+)\)/g, function (match, key) {
                //match means the whole pattern like @(name), key means the name
                return data[key];
            })
        },

        bindTemplate: function (data, id, template) {
            var html = template(template, data);
            document.getElementById(id).innerHTML = html;
            return html;
        },

        artTemplate: function(str, data) {
            var render = template.compile(str);
            return render(data);
        }
    });

    /**
     * string framework
     */
    $$extend.extend(w.$$, {
        //left trim
        ltrim: function(str) {
            //match 0 or more space start the beginning(use ^), () means the matched pattern
            return str.replace(/(^\s*)/g, '');
        },
        //right trim
        rtrim: function(str) {
            //match 0 or more space in the end(use $)
            return str.replace(/(\s*$)/g, '');
        },
        //left trim and right trim
        trim: function(str) {
            return str.replace(/(^\s*)|(\s*$)/g, '');
        },
        //capitalize the word
        capitalize: function (str) {
            return str.replace(/\b\w+\b/g, function (word) {
                //\b means the boundary of words
                return word.substring(0, 1).toUpperCase() + word.substring(1);
            });
        }
    });

    /**
     * select framework
     * w.$$ and Kang.prototype is different
     * if want to use $$.$id, then use w.$$, if want to use chain then Kang.prototype
     */
    $$extend.extend(w.$$, {
        $id: function (id) {
            return document.getElementById(id);
        },
        //get elements by tag, optional context is the parent element id
        $tag: function(tag, context) {
            if (typeof context == 'string') {
                context == $$.$id(context);
            }

            if (context) {
                return context.getElementsByTagName(tag);
            } else {
                return document.getElementsByTagName(tag);
            }
        },
        //get elements by class name
        $class: function(className, context) {
            if (typeof context == 'string') {
                context = $$.$id(context);
            }
            context = context || document;
            if (document.getElementsByClassName) {
                return context.getElementsByClassName(className);
            } else {
                //get all elements
                var elements = context.getElementsByTagName('*');
                var arr = [];
                //filter
                for (var i = 0, len = elements.length; i < len; i++) {
                    var classNames = elements[i].className.split(" ");
                    for(var j=0;j<classNames.length;j++) {
                        if (classNames[j] == className) {
                            arr.push(elements[i]);
                        }
                    }
                }

                return arr;
            }
        },
        //group selector
        $group: function(group) {
            //get single selectors
            var arr = [], result = [];
            arr = $$.trim(group).split(',');
            //find elements by single selector
            for(var i=0,len=arr.length;i<len;i++) {
                var item = $$.trim(arr[i]); //get rid of space first
                var first = item.charAt(0);
                var index = item.indexOf(first);
                var selector = item.slice(index+1);
                if (first === '.') {
                    //class selector
                    var elements = $$.$class(selector);
                    pushToArray(elements);
                } else if (first === '#') {
                    //id selector
                    var elements = [$$.$id(selector)];
                    pushToArray(elements);
                } else {
                    //tag selector
                    var elements = document.getElementsByTagName(item);
                    pushToArray(elements);
                }
            }
            function pushToArray(elements) {
                for (var j = 0; j < elements.length; j++) {
                    result.push(elements[i]);
                }
            }
            return result;
        },
        //filter, like $('#container div p') $('.container div p')
        $filter: function(filter) {
            var selectors = $$.trim(filter).split(' ');
            var result = [], context = [];
            //pipe, the result of upper level will be the input of next level
            for(var i=0, len=selectors.length;i<len;i++) {
                result = [];
                var item = $$.trim(selectors[i]);
                var first = item.charAt(0);
                var index = item.indexOf(first);
                var selector = item.slice(index+1);
                if (first === '#') {
                    //id selector
                    result = [$$.$id(selector)];
                } else if (first === '.') {
                    //class selector
                    if (context.length) {
                        for(var j=0, contextLen=context.length;j<contextLen;j++) {
                            pushToArray($$.$class(selector, context[j]));
                        }
                    } else {
                        pushToArray($$.$class(selector));
                    }
                } else {
                    //tag selector
                    if (context.length) {
                        for(var j=0, contextLen=context.length;j<contextLen;j++) {
                            pushToArray($$.$tag(item, context[j]));
                        }
                    } else {
                        pushToArray($$.$tag(item));
                    }

                }
                context = result;
            }

            return result;

            function pushToArray(elements) {
                for (var j = 0; j < elements.length; j++) {
                    result.push(elements[i]);
                }
            }
        }

    });

    /**
     * event framework
     */
    $$extend.extend(Kang.prototype, {
        //on method: add listeners on doms
        on: function (type, fn) {
            if (this.elements[0].addEventListener) {
                for (var i = 0; i < this.elements.length; i++) {
                    this.elements[i].addEventListener(type, fn, false);
                }
                return this;
            } else if (this.elements[0].attachEvent) {
                //ie 6
                for (var i = 0; i < this.elements.length; i++) {
                    this.elements[i].attachEvent('on' + type, fn);
                }
                return this;
            }
        },
        click: function(fn) {
            return this.on('click', fn);
        },
        mouseover: function(fn) {
            return this.on('mouseover', fn);
        },
        mouseout: function(fn) {
            return this.on('mouseout', fn);
        },
        hover: function(fnOver, fnOut) {
            if (fnOver) {
                return this.on('mouseover', fnOver);
            }
            if (fnOut) {
                return this.on('mouseout', fnOut);
            }
        },
        //get event object
        getEvent: function (e) {
            return e ? e : window.event;
        },
        //get event target
        getTarget: function (e) {
            var event = this.getEvent(e);
            return event.target || event.srcElement;
        },
        //prevent default behaviour
        preventDefault: function(e) {
            var event = this.getEvent(e);
            if (event.preventDefault) {
                event.preventDefault();
            } else {
                event.returnValue = false;
            }
        },
        //prevent event bubbling
        stopPropagation: function(e) {
            var event = this.getEvent(e);
            if (event.stopPropagation) {
                event.stopPropagation();
            } else {
                event.cancelBubble = true;
            }
        },
        //delegate
        delegate: function(eventType, selector, fn) {
            for(var i=0;i<this.elements.length;i++) {
                var parent = this.elements[i];
                function handle(e) {
                    var target = $$.getTarget(e);
                    console.log(target.nodeName);
                    if (target.nodeName.toLowerCase() === selector || target.id === selector || target.className.indexOf(selector) != -1) {
                        //when event bubbling, iterate all descendants, if found, execute fn
                        //why use call, can can change this(window) to target(dom)
                        fn.call(target);
                    }
                }
                //bind eventType to parent
                // parent[eventType] = handle; //use onclick eventType
                parent.addEventListener('click', handle);
            }
            return this;
        }

    });

    /**
     * css framework use $('#id').
     */
    $$extend.extend(Kang.prototype, {
        //hide the selected doms
        hide: function () {
            for (var i = 0; i < this.elements.length; i++) {
                this.elements[i].style.display = 'none';
            }
            return this;
        },
        //show the selected doms
        show: function () {
            for (var i = 0; i < this.elements.length; i++) {
                this.elements[i].style.display = 'block';
            }
            return this;
        },
        //css selector
        css: function (key, value) {
            var doms = this.elements;
            if (doms.length) {
                if (value) {
                    //set css style
                    setStyle(doms, key, value);
                } else {
                    //get style
                    return getStyle(doms[0], key);
                }
            } else {
                if (value) {
                    //set css style
                    doms.style[key] = value;
                } else {
                    //get style
                    return getStyle(doms, key);
                }
            }

            //set style
            function setStyle(doms, key, value) {
                for (var i = 0; i < doms.length; i++) {
                    doms[i].style[key] = value;
                }
            }
            //get style
            function getStyle(dom, key) {
                if (dom.currentStyle) {
                    return dom.currentStyle[key];
                } else {
                    return window.getComputedStyle(dom, null)[key];
                }
            }
        },
        //element width
        width: function() {
            return this.elements[0].clientWidth;
        },
        //element height
        height: function() {
            return this.elements[0].clientHeight;
        },
        //scroll width and height
        scrollWidth: function() {
            return this.elements[0].scrollWidth;
        },
        scrollHeight: function() {
            return this.elements[0].scrollHeight;
        },
        scrollTop: function() {
            return this.elements[0].scrollTop;
        },
        scrollLeft: function() {
            return this.elements[0].scrollLeft;
        }
    });

    /**
     * css framework use $$.
     */
    $$extend.extend(w.$$, {
        //document width
        documentWidth: function() {
            return document.documentElement.clientWidth;
        },
        //document height
        documentHeight: function() {
            return document.documentElement.clientHeight;
        },
        //document scroll width
        documentScrollWidth: function() {
            return document.body.scrollWidth;
        },
        //document scroll height
        documentScrollHeight: function() {
            return document.body.scrollHeight;
        },
        //screen width
        screenWidth: function() {
            return window.screen.width;
        },
        //screen height
        screenHeight: function() {
            return window.screen.height;
        }
    });

    /**
     * tab
     */
    $$extend.extend(Kang.prototype, {
        //tab
        tab: function(etype) {
            for(var i=0;i<this.elements.length;i++) {
                var tab = this.elements[i];
                var nav = tab.children[0];
                var contents = tab.children[1].children;
                var navItems = nav.children;

                for(var j=0;j<navItems.length;j++) {
                    var item = navItems[j];
                    item.setAttribute('indexForTabs', j);
                    etype = etype || 'mouseover';
                    w.$$.on(item, etype, function() {
                        var that = this;

                        for(var k=0;k<navItems.length;k++) {
                            //remove active class
                            navItems[k].className = navItems[k].className ? navItems[k].className.replace('active', '') : '';
                            contents[k].className = contents[k].className ? contents[k].className.replace('active', '') : '';
                        }
                        that.className = that.className + ' active';
                        var index = that.getAttribute('indexForTabs');
                        contents[index].className = contents[index].className + ' active';
                    });
                }

            }
            return this;
        }
    });

    /**
     * relationship selector
     */
    $$extend.extend(Kang.prototype, {
        /**
         * parent selector
         * get the direct parent
         * 
         * usage: $$('#id').parent()
         */
         parent: function() {
             if (this.elements.length >= 1) {
                 return new Kang(this.elements[0].parentNode);
             } else {
                 return null;
             }
         },
         /**
          * sibling selector
          * get the next sibling
          * 
          * usage: $$('#id').next()
          */
          next: function() {
              if (this.elements.length >= 1) {
                  var ele = this.elements[0];
                  var next = ele.nextElementSibling || ele.nextSibling;
                  return new Kang(next);
              } else {
                  return null;
              }
          },
          /**
           * sibling selector
           * get the previous sibling
           * 
           * usage: $$('#id').prev()
           */
          prev: function() {
              if (this.elements.length >= 1) {
                  var ele = this.elements[0];
                  var prev = ele.previousElementSibling || ele.previousSibling;
                  return new Kang(prev);
              } else {
                  return null;
              }
          },
          /**
           * children selector
           * get the children nodes
           * 
           * usage: $$('#id').children()
           */
          children: function() {
              if (this.elements.length >= 1) {
                  var ele = this.elements[0];
                  this.elements = [];
                  this.elements = ele.children;
                  return this;
              } else {
                  return null;
              }
          },
          /**
           * first element selector
           * get the first element 
           * 
           * Usage: $$("li").first()
           * 
           * @return {element} element
           */
          first: function() {
            if (this.elements.length >= 1) {
                var ele = this.elements[0];
                this.elements = [];
                this.elements.push(ele);
                return this;
            } else {
                return null;
            }
          }
    });

    /**
     * node manipulation
     */
    $$extend.extend(Kang.prototype, {
        /**
         * append child
         * 
         * usage: $$('#id').append('div')
         */
        append: function(tag) {
            var ele = null;
            if (typeof tag === 'object') {
                if (tag instanceof Kang) {
                    ele = tag.elements[0];
                } else {
                    ele = tag;
                }
            } else {
                //element tag
                ele = document.createElement(tag);
                //todo <div></div>
            }
            for(var i=0;i<this.elements.length;i++) {
                this.elements[i].appendChild(ele);
            }
            return this;
        },
        /**
         * append child in the first place
         * 
         * usage: $$('#id').prepend('div')
         */
        prepend: function(tag) {
            var ele = null;
            if (typeof tag === 'object') {
                if (tag instanceof Kang) {
                    ele = tag.elements[0];
                } else {
                    ele = tag;
                }
            } else {
                var ele = document.createElement(tag);
            }
            
            for(var i=0;i<this.elements.length;i++) {
                var children = this.elements[i].children;
                this.elements[i].insertBefore(ele,children[0]);
            }
            return this;
        },
        /**
         * remove element
         * 
         * usage: $$('#id').remove(), $$('#id').remove(child)
         */
        remove: function(selector) {
            if (selector) {
                //remove child
                if (typeof selector === 'object') {
                    for(var i=0;i<this.elements.length;i++) {
                        this.elements[i].removeChild(selector);
                    }
                } else {
                    for(var i=0;i<this.elements.length;i++) {
                        var doms = this.elements[i].querySelectorAll(selector);
                        for(var j=0;j<doms.length;j++) {
                            this.elements[i].removeChild(doms[j]);
                        }
                    }
                }
                return this;
            } else {
                //remove itself
                for(var i=0;i<this.elements.length;i++) {
                    this.elements[i].parentNode.removeChild(this.elements[i]);
                }
            }
        },
        /**
         * clone a node
         * 
         * usage: $$("#id").clone(true) or $$("#id").clone()
         * 
         * @param {boolean} deepCopy true or false
         * @return the cloned node
         */
         clone: function(deepCopy) {
            if (this.elements.length >= 1) {
                return this.elements[0].cloneNode(deepCopy);
            } else {
                return null;
            }
         }
    });

    /**
     * attribute framework
     */
    $$extend.extend(Kang.prototype, {
        /**
         * attribute
         * get or set attribute
         * 
         * Usage: $$("#id").attr("class") or $$("#id").attr("class", "circle")
         * @param {string} attr attribute name
         * @return {string} attribute value or nothing for setting attribute
         */
        attr: function(attr, value) {
            if (value) {
                //set attribute
                for(var i=0;i<this.elements.length;i++) {
                    this.elements[i].setAttribute(attr, value);
                }
            } else {
                //get attribute
                if (this.elements.length >= 1) {
                    return this.elements[0].getAttribute(attr);
                } else {
                    return null;
                }
            }
        },
        /**
         * remove attribute
         * 
         * Usage: $$("#id").removeAttr(attr)
         * 
         * @param {string} attr attribute name
         */
        removeAttr: function(attr) {
            for(var i=0;i<this.elements.length;i++) {
                this.elements[i].removeAttribute(attr);
            }
        },
        /**
         * html
         * get or set html
         * 
         * Usage: $$("#id").html() or $$("#id").html(html)
         * 
         * @param {string} html
         * @return {string} innerHTML 
         */
        html: function(html) {
            if (html) {
                //set html
                for(var i=0;i<this.elements.length;i++) {
                    this.elements[i].innerHTML = html;
                }
            } else {
                //get html
                if (this.elements.length >= 1) {
                    return this.elements[0].innerHTML;
                } else {
                    return null;
                }
            }
        },
        /**
         * text
         * get or set text in element node
         * 
         * Usage: $$("#id").text() or $$("#id").text(txt)
         * @param {string} text text content
         * @return {string} text
         */
        text: function(txt) {
            if (txt) {
                //set text
                for(var i=0;i<this.elements.length;i++) {
                    this.elements[i].innerText = txt;
                }
            } else {
                //get text
                if (this.elements.length >= 1) {
                    return this.elements[0].innerText;
                } else {
                    return null;
                }
            }
        }

    });
})(window);
